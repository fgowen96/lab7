from django.shortcuts import render
from django.views import View
from LabApp7.models import Card
from LabApp7.models import User
# Create your views here.


class Home(View):
    def get(self,request):
        return render(request, 'main/index.html')

    def post(self,request):
        yourinstance = Card()
        commandinput = request.POST["command"]
        userName = request.POST["user"]
        if commandinput:
            response = yourinstance.command(userName+":"+commandinput)
        else:
            response = ""
        return render(request, 'main/index.html',{"message":response})

