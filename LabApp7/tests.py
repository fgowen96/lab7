from django.test import TestCase

from unittest import mock
from LabApp7.models import Card
from LabApp7.models import User
from LabApp7.models import Deck


# Create your tests here.
def mockDeckGenerator():
    ############################################
    # Creates a valid mock deck, validMockDeck
    ############################################
    validMockDeck = []
    for currentSuit in range(1, 5):  # range is not inclusive
        for currentRank in range(1, 14):
            mockCard = mock.Mock()
            mockCard.getSuit = mock.Mock(return_value=currentSuit)
            mockCard.getRank = mock.Mock(return_value=currentRank)
            validMockDeck.append(mockCard)

    return validMockDeck
    ############################################

# Card tests
class CardInitCorrectRankAndSuit(TestCase):
    def test_GetRankAndGetSuitAccuracy(self):
        # create card
        fakeCard = Card(3, 4)  # create a card whose suit is 3 and rank is 4
        # test(s)
        self.assertEqual(fakeCard.getSuit(), 3)  # check that, if the card's suit is 3, its getSuit() method returns 3
        self.assertEqual(fakeCard.getRank(), 4)  # check that, if the card's rank is 4, its getRank() method returns 4
        self.assertNotEqual(fakeCard.getSuit(), 4)  # check that getSuit() does not return rank
        self.assertNotEqual(fakeCard.getRank(), 3)  # check that getRank() does not return suit

class CardInitRaiseValueError(TestCase):
    def test_RaiseValueError(self):
        self.testCard = Card(1,1)
        self.assertEqual(0, self.testCard.getValue()) # more needed once Poker Card Values Set

class UserInit(TestCase):
    def test_Init(self):
        user1 = User
        user1.name = 'Junpei'
        user1.highScore = 5
        self.assertEqual(user1.name, 'Junpei')
        self.assertEqual(user1.highScore, 5)

class UserNameQueryMatch(TestCase):
    def test_NameQueryMatch(self):
        users = User.objects.all()
        names = User.objects.values_list('name')
        self.assertEqual(names(1),'Lotus')





#AFTER MIGRATION STUFF, TYPE test <AppName> TO SEE IF THE TESTS WORK, I CHANGED FAKECARD FROM (3,4) TO (3,3), BE CAREFUL
#ENSURE THAT MODELS IS EXACTLY THE SAME BUT models.Model
#NOT unittest.TestCase, just TestCase



